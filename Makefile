BUILDIR = .
CLASSPATH = CLASSPATH=$(shell pwd)
JAVAC = $(CLASSPATH) javac -Xlint -d $(BUILDIR) 
RMIC = rmic -d $(BUILDIR) -classpath $(BUILDIR) -nowarn

all:
	$(JAVAC) src/*.java
	$(RMIC) JeuImpl
	$(RMIC) EntiteImpl
	$(RMIC) AgentImpl

clean:
	rm -f build/*.class
