/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

import java.rmi.* ; 
import java.net.* ;
import java.util.ArrayList;

/** Classe abstraite de l'agent
 */
public interface Agent extends Entite{

  /** Recolte des ressources chez un producteur spécifique
   * 
   * @param vProd : Le producteur auprès duquel il faut recolter
   * @return  La quantité de ressource recolté
   * @throws java.rmi.RemoteException
   */
  public int recolter(Producteur vProd) throws RemoteException;

  /** Vol des ressources chez un agent spécifique
   * 
   * @param vAgent : L'agent auprès duquel il faut essaye de voler
   * @return La quantité de ressources volées
   * @throws java.rmi.RemoteException
   */
  public int voler(Agent vAgent) throws RemoteException;

  /** Demande à l'agent de se connecter aux producteurs donnés
   * 
   * @param producteurs : adresses des producteurs du jeu
   */
  public void connexionProducteurs(ArrayList<String> producteurs) throws RemoteException
    , MalformedURLException
    , NotBoundException;

  /** Demande à l'agent de se connecter aux agents donnés
   * 
   * @param agents : adresses des agents du jeu
   */
  public void connexionAgents(ArrayList<String> agents) throws RemoteException
    , MalformedURLException
    , NotBoundException;

  /** Getter de l'état d'observation
	 *
	 * @return le booléan d'observation de l'agent
	 */
	public boolean getEtat() throws RemoteException;

}
