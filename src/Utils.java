/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

import java.util.Scanner;
import java.net.MalformedURLException;
import java.rmi.* ;



/** Classe utilitaire */
public class Utils{

   private static final Scanner reader = new Scanner(System.in);

  /** Demande à l'utilisateur de faire un choix
   *
   * Soit en entrant directement la valeur d'un choix, soit en entrant l'indice du choix
   *
   * @param question    La question auquelle doit répondre l'utilisateur
   * @param choices     Les choix possibles
   * @param default     L'indice du choix par défaut (celui choisis si l'usser ne rentre rien
   *                    -1 si aucun choix par défaut
   * @return L'index du choix de l'utilisateur
   */
  public static int choixTerminal(String question
      , String [] choices
      , int defaultChoice){

    System.out.println(question);

    String choicesOutput = "";
    boolean goToLine = choices[0].length() > 1;
    String pre = "";
    for(int i = 0; i < choices.length; i++){

      if(goToLine){
        if(i == defaultChoice) choicesOutput += "[" + (i+1) + "]: " + choices[i];
        else choicesOutput += (i+1) + ": " + choices[i];
        
        if(i+1 < choices.length) choicesOutput += "\n";
      }else{
        if(i == defaultChoice) choicesOutput += "[" + choices[i] + "]";
        else choicesOutput += choices[i];

        if(i+1 < choices.length) choicesOutput += " / ";
      }
    }
    System.out.println(choicesOutput);

    String userinput;
    boolean ok = false;

    while(!ok){

      userinput = reader.nextLine();

      if(userinput.equals("") && defaultChoice >= 0){
          return defaultChoice;
      }

      for(int i = 0; i < choices.length; i++){
        if(userinput.equals(choices[i])) return i;
      }

      int idChoice = Integer.parseInt(userinput);
      if(idChoice > 0 && idChoice <= choices.length){
        return idChoice-1;
      }
      System.out.println("Valeur incorrecte..\n" + choicesOutput);
    }

    return -1;
  }

  public static int choixTerminal(String question, String [] choices){
    return choixTerminal(question, choices, -1);
  }

  /** Demande a l'utilisateur de choisir une valeur entière
   *
   * @param question    La question posé à l'utilisateur
   * @param valeurDefaut     La valeur utilisée par défaut si l'utilisateur ne rentre rien
   * @param useValeurDefaut  Permet de definir si oui ou non la valeur par défaut est utilisé
   * @param condition   Condition utilisé pour valider la valeur de l'user
   * @return La valeur choisis par l'utilisateur
   */
  private static int choixTerminalInt(String question
      , int valeurDefaut
      , boolean useValeurDefaut
      , conditionTerm.Num condition){
    if(useValeurDefaut) System.out.print(question + " (" + valeurDefaut + " par défaut)");
    else System.out.print(question);

    String userinput;
    boolean ok = false;

    while(!ok){

      userinput = reader.nextLine();

      if(userinput.equals("") && useValeurDefaut){
          return valeurDefaut;
      }

      try{
        int v = Integer.parseInt(userinput);
        if(condition != conditionTerm.Num.ALL &&
            (
             condition == conditionTerm.Num.POSITIVE && v > 0
             || condition == conditionTerm.Num.NEGATIVE && v < 0
             || condition == conditionTerm.Num.POSITIVEOUNULLE && v >= 0
             || condition == conditionTerm.Num.NEGATIVEOUNULLE && v <= 0
	     || condition == conditionTerm.Num.NONNULLE && (v < 0 || v >0)
            )){
              return v;
            }
      }catch(NumberFormatException e){
      }
      switch(condition){
	      case NEGATIVE:
		      System.out.println("Une valeur negative est requise");
		      break;
	      case POSITIVE:
		      System.out.println("Une valeur positive est requise");
		      break;
	      case NONNULLE:
		      System.out.println("Une valeur non nulle est requise");
		      break;
	      case POSITIVEOUNULLE:
		      System.out.println("Une valeur >= 0 est requise");
		      break;
	      case NEGATIVEOUNULLE:
		      System.out.println("Une valeur <= 0 est requise");
		      break;
      }

    }

    return -1;

  }

  public static int choixTerminalInt(String question, int valeurDefaut, conditionTerm.Num cond){
    return choixTerminalInt(question, valeurDefaut, true, cond);
  }

  public static int choixTerminalInt(String question, conditionTerm.Num cond){
    return choixTerminalInt(question, -1, false, cond);
  }

  public static int choixTerminalInt(String question, int valeurDefaut){
    return choixTerminalInt(question, valeurDefaut, true, conditionTerm.Num.ALL);
  }

  public static int choixTerminalInt(String question){
    return choixTerminalInt(question, -1, false, conditionTerm.Num.ALL);
  }

  public static void stop(){
    System.exit(1);
  }

  /** Essaye de bind une entité ou plante
   *
   * @param e L'entité qu'il faut bind
   */
  public static void bindEntiteOrFail(EntiteImpl e){
    try
    {
      Naming.bind("rmi://" + e.getAddr() , e) ;
      System.out.println(e.nom() + " prêt à l'adresse " + e.getAddr() + ".");
    }catch(AlreadyBoundException abe){
      System.out.println(abe);
      System.out.println("Une entité a déjà été créé à cette adresse avec cet id, "
          + "veuillez réessayer.");
      Utils.stop();
    }catch(AccessException ae){
      System.out.println(ae);
      System.out.println("Enregistrement impossible sur cette adresse.");
      Utils.stop();
    }catch(RemoteException re){
      System.out.println(re);
      System.out.println("Enregistrement de l'entité non autorisé.");
      Utils.stop();
    }catch(MalformedURLException npe){
      System.out.println(npe);
      System.out.println("Adresse ou nom incorrect");
      Utils.stop();
    }
  }

  /** Essaye de unbind un objet via son adresse
   *
   * @param addr  L'adresse rmi de l'objet
   */
  public static void unbindObjectOrFail(String addr){
    try
    {
      Naming.unbind("rmi://" + addr) ;
      System.out.println("Objet " + addr + " retirée de rmi.");
    }catch(NotBoundException nbe){
      System.out.println(nbe);
      System.out.println("Aucun objet n'existe à cette adresse(" + addr + ").");
      Utils.stop();
    }catch(AccessException ae){
      System.out.println(ae);
      System.out.println("Suppression impossible sur cette adresse(" + addr + ").");
      Utils.stop();
    }catch(RemoteException re){
      System.out.println(re);
      System.out.println("Impossible de contacter le registre de noms.");
      Utils.stop();
    }catch(MalformedURLException npe){
      System.out.println(npe);
      System.out.println("Adresse ou nom incorrect");
      Utils.stop();
    }
  }

  /** Essaye de récuperer le jeu ou plante
   *
   * @param rmiJeu L'adresse rmi du jeu
   * @return L'interface d'utilisation à distance du jeu
   */
  public static Jeu lookupJeuOrFail(String rmiJeu){
    Jeu j = null;
    // Lookup du jeu
    try{
      j = (Jeu) Naming.lookup("rmi://" + rmiJeu + "/Jeu");
    }catch(NotBoundException nbe){
      System.out.println(nbe);
      System.out.println("Aucun jeu n'est rattaché à cette adresse.");
      Utils.stop();
    }catch(RemoteException re){
      System.out.println(re);
      System.out.println("Une erreur est survenue lors de la prise de contact avec le jeu.");
      Utils.stop();
    }catch(MalformedURLException nbe){
      System.out.println(nbe);
      System.out.println("L'adresse fournit pour le jeu est incorrecte.");
      Utils.stop();
    }

    return j;
  }
}
