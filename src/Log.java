/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

import java.util.Date;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;
import java.rmi.* ;

/**
 * Permet d'enregistrer les données liés à une partie
 */
public class Log{

  String logPath;

  /** Les etats du jeu a chaque instants */
  private ArrayList<String> etats;

  /** Les stock des entites à chaque instant */
  private HashMap<String, ArrayList<Integer>> stocksEntites;

  /** Numéro de l'étape courante */
  private int etapeCourante;

  /**
   * Constructeur par défaut
   *
   * Créer le dossier pour les logs
   */
  public Log(){

    etats = new ArrayList<>();
    stocksEntites = new HashMap<>();
    etapeCourante = 0;

    logPath = System.getProperty("user.dir") + "/game_log";

    Date date = new Date(); // your date
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH);
    int day = cal.get(Calendar.DAY_OF_MONTH);

    logPath += "/game_" + year + "_" + month + "_" + day + "_";
    int current = 1;
    String prefix = logPath;

    // On recherche le premier indice dispo
    File f = new File(prefix + current);
    while(f.exists() && f.isDirectory()) {
      current++;
      f = new File(prefix + current);
    }
    logPath = prefix + current;

    System.out.println("La configuration et les résultats seront disponibles à la fin du jeu dans ce dossier: \n " + logPath + "\n");

    createFolder();

  }

  /**
   * Creer le dossier pour les logs
   */
  private void createFolder(){
    File dir = new File(logPath);
    try{
      dir.mkdirs();
    }catch(Exception e){
      System.out.println(e);
      System.out.println("Impossible de créer le dossier de log.");
      throw e;
    }
  }

  /**
   * Permet d'enregistrer une configuration donné dans le dossier de log
   */
  public void enregistrerConfig(Configuration conf) throws IOException{
    String dest = logPath + "/config.txt";
    conf.save(dest);
  }

  /**
   * Permet d'ajouter les donnes d'un instant
   *
   * @param etat, l'état du jeu
   * @param entites, Les entites du jeu
   */
  public void addState(String etat, ArrayList<Entite> entites){
    etats.add(etat);

    for(Entite e : entites){
      try{
        if(!stocksEntites.containsKey(e.nom())){
          stocksEntites.put(e.nom(), new ArrayList<>());
        }

        if(stocksEntites.get(e.nom()).size() < etapeCourante + 1){
          for(int i = 0; i <= etapeCourante; i++){
            stocksEntites.get(e.nom()).add(-1);
          }
        }
        stocksEntites.get(e.nom()).add(e.getStock());
      }catch(RemoteException re){
        System.out.println(re);
        System.out.println("Log: Impossible d'enregistrer le stock d'une entité.");

      }

    }

    etapeCourante++;
  }

  private void writeCSVLine(BufferedWriter bw, String[] line) throws IOException{
    for(int i = 0; i < line.length; i++){
      bw.write(line[i]);
      if(i + 1 < line.length) bw.write(";");
    }
    bw.write("\n");

  }

  /**
   * Enregistrer les logs en csv dans un fichier
   */
  public void saveLog() throws IOException{
    String csv = logPath + "/jeu.csv";
    BufferedWriter writer = new BufferedWriter(new FileWriter(csv));


    // Ecriture des colones
    int qtColones = 2 + stocksEntites.size();
    String [] colones = new String[qtColones];
    colones[0] = "Etape";
    colones[1] = "Etat du jeu";
    for(int i = 0; i < stocksEntites.size(); i++){
      String nom = (String) stocksEntites.keySet().toArray()[i];
      colones[2 + i] = "Stock ressources " + nom;

    }

    writeCSVLine(writer, colones);


    // Ecriture des donnes
    for(int i = 0; i < etapeCourante; i++){
      String [] data = new String[qtColones];
      data[0] = String.valueOf(i);
      data[1] = etats.get(i);
      for(int j = 0; j < stocksEntites.size(); j++){
        String nom = (String) stocksEntites.keySet().toArray()[j];
        data[2 + j] = String.valueOf(stocksEntites.get(nom).get(i));
      }

      writeCSVLine(writer, data);
    }

    writer.close();
  }


}
