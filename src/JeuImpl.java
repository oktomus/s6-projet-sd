/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

import java.rmi.server.UnicastRemoteObject ;
import java.rmi.* ;
import java.net.* ;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.locks.*;
import java.io.IOException;

/** Implémentation du gestionnaire du jeu
 */
public class JeuImpl
  extends UnicastRemoteObject
  implements Jeu
{  

  private static final long serialVersionUID = 40L;

  /** Configuration du jeu */
  private Configuration config;

  /** Agents du jeu */
  private ArrayList<Agent> agents;

  /** Producteurs du jeu */
  private ArrayList<Producteur> producteurs;

  /** Les entites du jeu */
  private ArrayList<Entite> entites;

  /** Adresse rmi des producteurs et des agents */
  private ArrayList<String> rmiProds, rmiAgents;

  /** Adresse et port du jeu */
  private String rmiAddr;

  /** Logger du jeu */
  private Log logger;

  /** L'id courament disponible
   * A chaque demande d'id, cette valeur est incrémenté,
   * cela permet de ne pas avoir 2 joueurs avec le meme id s'ils font la demande
   * en meme temps
   */
  private int availableId;

  /** Permet d'ouvrir ou fermer les connexions */
  private boolean salonOuvert;

  /** Token valant true une fois que le jeu est fini */
  private boolean tokenFini;

  /** Nom du gagnant */
  private String gagnant;

  /** Indice du tour courrant */
  private int tour;

  /** La dernière action effectuée par une entitée */
  private String derniereAction;

  /** Gestionnaire des conditions d'attente du jeu */
  private Lock lock;

  /** Condition utilisé pour attendre les entrées dans le salon */
  private Condition attenteSalon;
 
  /** Condition utilisée pour bloquer le jen en attendant la fin d'une action d'une entité */
  private Condition attenteTour;

  /** Constructeur par défaut 
   *
   * @param rmiAddr Adresse:port du jeu
   */
  public JeuImpl(String rmiAddr) throws RemoteException{
    config = new Configuration();
    agents = new ArrayList<>();
    producteurs = new ArrayList<>();
    rmiProds = new ArrayList<>();
    rmiAgents = new ArrayList<>();
    this.rmiAddr = rmiAddr + "/Jeu";
    salonOuvert = false;
    tokenFini = false;
    tour = 0;
    derniereAction = "";
    gagnant = "";
    availableId = 0;

    lock = new ReentrantLock();
    attenteSalon = lock.newCondition();
    attenteTour = lock.newCondition();
    logger = new Log();
  }

  /** Ouvre le salon des connexions du jeu
   */
  private void ouvrirSalon(){
    salonOuvert = true;
  }

  /** Retourne l'état du salon
   */
  private String salon(){
    int numAgent = agents.size();
    int numProd = producteurs.size();
    return "Salon : [" + numAgent + " agent(s), " + numProd + " producteur(s)]";
  }

  public Configuration getConfig() throws RemoteException{
    return config;
  }
  
  public int getAvailableId() throws RemoteException{
    availableId += 1;
    return availableId; // Non atomique
  }
  
  public void ajouterAgent(String rmiAg) 
    throws RemoteException
    , MalformedURLException
    , NotBoundException{

    if(!salonOuvert) throw new RemoteException("Le salon n'est pas ouvert");

    System.out.println("Connexion d'un agent en cours...");
    try{
      Agent agDistant = (Agent) Naming.lookup("rmi://" + rmiAg);
      agents.add(agDistant);
      rmiAgents.add(rmiAg);
    }catch(Exception e){
      System.out.println("Connexion de l'agent échouée.");
      throw e;
    }
    System.out.println("Connexion de l'agent réussite.");

    lock.lock();
    try{
      attenteSalon.signal();
    }finally{
      lock.unlock();
    }

  }

  public void ajouterProducteur(String rmiProd) 
    throws RemoteException
    , MalformedURLException
    , NotBoundException{

    if(!salonOuvert) throw new RemoteException("Le salon n'est pas ouvert");

  
    System.out.println("Connexion d'un producteur en cours...");
    try{
      Producteur prodDistant = (Producteur) Naming.lookup("rmi://" + rmiProd);
      producteurs.add(prodDistant);
      rmiProds.add(rmiProd);
    }catch(Exception e){
      System.out.println("Connexion du producteur échouée.");
      throw e;
    }
    System.out.println("Connexion du producteur réussite.");

    lock.lock();
    try{
      attenteSalon.signal();
    }finally{
      lock.unlock();
    }

  }

  public void signalerFinAction(String action) throws RemoteException{

    derniereAction = action;
    logger.addState(etat(), entites);
    lock.lock();
    try{
      attenteTour.signal();
    }finally{
      lock.unlock();
    }

  }

  public boolean estFini(){

    return tokenFini;

  }

  public String gagnant(){
    return gagnant;
  }

  public String etat(){
    if(tokenFini) return "Jeu terminé, " 
      + derniereAction + ", le gagnant est " + gagnant + " !";
    else return "Tour " + tour + ": " + derniereAction ;
  }

  /** Démarrer le jeu et lance les tours
   */
  private void demarrerJeu() throws RemoteException{

    entites = new ArrayList<>(agents);
    entites.addAll(producteurs);

    // On donne la configuration à chaque entité
    for(Producteur p : producteurs)
      p.init(config);

    for(Agent a : agents) {
      a.init(config);
    }

    try{
      for(Agent a : agents) {
        // On donne la liste des producteurs aux agents
        try{
          a.connexionProducteurs(rmiProds);
        }catch(Exception e){
          System.out.println("L'agent " + a.nom() + " n'a pas pu se connecter aux producteurs.");
        }
      }
    }catch(Exception e){
      Utils.stop();
    }

    try{
      // On donne la liste des agents aux agents (sauf lui-même)
      ArrayList<String> tempAgents = new ArrayList<String>();
      for(int i = 0; i < agents.size(); i++) {
        for(int j = 0; j < rmiAgents.size(); j++) {
          if(i != j) {
            try{
              tempAgents.add(rmiAgents.get(j));
            } catch(Exception e){
              System.out.println("L'agent " + agents.get(i).nom() + " n'a pas pu se connecter aux agents.");
            }
            agents.get(i).connexionAgents(tempAgents);
            tempAgents.clear();
          }
        }
      }
    }catch(Exception e){
      Utils.stop();
    }


    // Premier etat du jeu
    derniereAction = "Jeu initialisé.";
    logger.addState(etat(), entites);

    int agentCourant;
    int condVict = config.getParam("conditionVictoire");
    boolean fin = false;

    while(!fin){
      tour++;
      for(int i = 0; i < agents.size(); i++){
        agentCourant = i;

        // Faire produire chaque producteurs
        for(Producteur p : producteurs){
          faireJouerEntite(p);
        }

        // Faire jouer l'agent courant
        faireJouerEntite(agents.get(agentCourant));

        // Signaler tout le monde de l'action sauf l'agent courant
        for(Producteur p : producteurs){
          p.signalerFinTour(); 
        }

        for(int j = 0; j < agents.size(); j++){
          if(j != agentCourant) agents.get(i).signalerFinTour();
        }
      }

      // Check de la fin du jeu
      fin = false; // Ne pas agir directement sur token
      if(condVict == 3 || condVict == 1){ // On check que tous les prod soient vides
        fin = true;
        for(Producteur p : producteurs){
          if(p.getStock() > 0){
            fin = false;
          }
        }
        if(fin) derniereAction = "tous les producteurs sont vides";
      }
      if(!fin && (condVict == 3 || condVict == 2)){
        // On check qu'un agent ait atteint le stock victoire
        int stockVict = config.getParam("stockVictoireAgent");
        for(Agent a : agents){
          if(a.getStock() >= stockVict){
            fin = true;
            derniereAction = "stock de ressources victoire atteint";
          }
        }
      }

    }

    Agent top = agents.get(0);
    for(Agent a : agents){
      if(a.getStock() > top.getStock()) top = a;
      break;
    }
    this.gagnant = top.nom();
    tokenFini = true;
    
    logger.addState(etat(), entites);

    // On signale tout le monde que c'est fini
    for(Producteur p : producteurs){
      p.signalerFinTour(); 
      p.stop();
    }

    for(Agent a : agents){
      a.signalerFinTour();
      a.stop();
    }

    System.out.println(etat());

    try{
      logger.enregistrerConfig(config);
    }catch(IOException e){
      System.out.println(e);
      System.out.println("Impossible d'enregistrer la configuration.");
    }

    // Attendre que toutes les entites se soient déco

    boolean canStop = false;

    
    while(!canStop){
      canStop = true;
innerloop:
      for(Entite e: entites){
        try{
          e.getStock();
          canStop = false;
          break innerloop;
        }catch(RemoteException re){

        }
      }
      try{
        Thread.sleep(500);
      }catch(Exception e){
      }
    }

    try{
      logger.saveLog();
    }catch(IOException ioe){
      System.out.println(ioe);
      System.out.println("Impossible d'enregistrer les log du jeu.");
    }
  }

  /** Permet de faire jouer une entité et attend quelle ait fini
   *
   * @param e  L'entite à faire jouer
   */
  private void faireJouerEntite(Entite e) throws RemoteException{
    lock.lock();
    try{
      e.lancerTour();
      attenteTour.await();
    }catch(RemoteException er){
      System.out.println("Impossible de faire jouer l'entité");
      throw er;
    }catch(InterruptedException ie){
    }finally{
      lock.unlock();
    }
    System.out.println(etat());
  }


  public static void main(String [] args){


    if(args.length < 1 || args.length > 2){
      System.out.println("USAGE : java JeuImpl adrJeu:portJeu configFile");
      Utils.stop();
    }

    String configFile = null;
    if(args.length == 2) configFile = args[1];
   

    String rmiAddr = args[0];

    JeuImpl objLocal = null;

    try{
      objLocal = new JeuImpl(rmiAddr);
    }catch(RemoteException re){
      System.out.println(re);
      System.out.println("Impossible d'instancier le jeu.");
      Utils.stop();
    }

    // Bind
    try
    {
      Naming.bind("rmi://" + objLocal.rmiAddr ,objLocal) ;
      System.out.println("Jeu prêt à l'adresse " + rmiAddr + ".");
    }catch(AlreadyBoundException abe){
      System.out.println(abe);
      System.out.println("Un jeu a déjà été créé à cette adresse.");
      Utils.stop();
    }catch(AccessException ae){
      System.out.println(ae);
      System.out.println("Enregistrement impossible sur cette adresse.");
      Utils.stop();
    }catch(RemoteException re){
      System.out.println(re);
      System.out.println("Enregistrement du jeu non autorisé.");
      Utils.stop();
    }catch(MalformedURLException npe){
      System.out.println(npe);
      System.out.println("Adresse ou nom incorrect");
      Utils.stop();
    }

    try{
      
      if(configFile != null) objLocal.getConfig().demarrerConfiguration(configFile);
      else objLocal.getConfig().demarrerConfiguration();

    }catch(IOException | ClassNotFoundException e){
      System.out.println(e);
      System.out.println("Impossible de lire le fichier de configuration.");
    }


    try{

      objLocal.ouvrirSalon();
      System.out.println("Ouverture du salon");

      boolean start = false;
      int choixUser;


      while(!start){
        objLocal.lock.lock();
        try{
          System.out.println("En attente d'une connexion...");
          objLocal.attenteSalon.await();
          System.out.println(objLocal.salon());


          if(objLocal.agents.size() > 0 && objLocal.producteurs.size() > 0){
            choixUser = Utils.choixTerminal("Voulez vous démarrer le jeu ?", new String[]{"y", "n"}, 0);
            if(choixUser == 0) start = true;
          }

        }finally{
          objLocal.lock.unlock();
        }
      }

      objLocal.salonOuvert = false;

      System.out.println("Lancement du jeu");
      objLocal.demarrerJeu();
      System.out.println("Jeu terminé");


      Utils.unbindObjectOrFail(objLocal.rmiAddr);
    }
    catch (RemoteException re) { System.out.println(re) ; }
    catch (InterruptedException re) { System.out.println(re) ; }
    System.exit(0);

  }


}
