/**
 * Joseph Stich
 * Kevin Masson
 * L3 Informatique
 * S6 - Printemps 2017
 * Université de Strasbourg
 */

import java.rmi.server.UnicastRemoteObject ;
import java.rmi.* ;
import java.net.* ;
import java.util.ArrayList;

/** Interface pour la gestion du jeu à distance
 */
public interface Jeu extends Remote{

  /** Retourne la configuration du jeu
   *
   * @return    L'object Configuration du jeu actuel
   */
  public Configuration getConfig() throws RemoteException;

  /** Retourne le prochain id libre */
  public int getAvailableId() throws RemoteException;

  /** Ajoute un agent au jeu
   *
   * @param rmiAg   L'adresse complete de l'agent
   */
  public void ajouterAgent(String rmiAg) 
    throws RemoteException
    , MalformedURLException
    , NotBoundException;
 
  /** Ajoute un producteur au jeu
   *
   * @param rmiProd L'adresse complete du prod
   * @return Le numéro du producteur dans le jeu, -1 si non ajouté
   */
  public void ajouterProducteur(String rmiProd) 
    throws RemoteException
    , MalformedURLException
    , NotBoundException;

  /** Signale au jeu qu'une entité à fini une certainne action
   *
   * @param action    L'action executé par l'entité
   *                  Son nom doit paraître dedans
   */
  public void signalerFinAction(String action) throws RemoteException;

  /** Permet de savoir si le jeu est fini
   *
   * @return  True si le jeu est fini
   */
  public boolean estFini() throws RemoteException;

  /** Retourne l'état du jeu
   *
   * @return Une chaine montrant l'état global du jeu
   */
  public String etat() throws RemoteException;

  /** Retourne le nom du gagnant
   *
   * Ou "" si le jeu n'est pas fini ou si il n'y a pas de gagnant
   * @return Une chaine designant le nom du gagnant
   */
  public String gagnant() throws RemoteException;


}
