
import java.rmi.* ; 
import java.net.MalformedURLException ; 

public class Test
{
  public static void main(String [] args)
  {
    try{
      Jeu j = (Jeu) Naming.lookup("rmi://localhost:8000/Jeu") ;
      Configuration conf = j.getConfig();
      System.out.println("Configuration init : " + conf.getParam("init"));
    }
    catch (NotBoundException re) { System.out.println(re) ; }
    catch (RemoteException re) { System.out.println(re) ; }
    catch (MalformedURLException e) { System.out.println(e) ; }
  }
}
