**Toutes les commandes doivent êtres lancées depuis le dossier racine**

# Compilation

Il suffit de lance la commande `make`

# Démarrer le serveur rmi

Par défaut, tout est configurer pour être lancé sur le localhost et le port 8000. Vous pouvez donc juste lancer un seul rmiregistry sur le port 8000 de cette façon :

`rmiregistry 8000 [&]`

Si vous lancez les différentes entités du jeu sur différents serveur, pensez à lancer rmiregistry avec chaque port utilisé.

# Démarrer le jeu

`java JeuImpl adresse:port`

Ou, avec un configuration

`java JeuImpl adresse:port fichier`

Les configurations déjà écrites sont dans le dossier config.

# Ajouter un agent

`java AgentImpl adresseJeuDistant:portJeuDistant adresseAgent:portAgent`

# Ajouter un prodcteur

`java ProducteurImpl adresseJeuDistant:portJeuDistant adresseProducteur:portProducteur`

# Nettoyer les fichiers de compilation

`make clean`

